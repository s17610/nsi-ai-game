"""
==============================
Movie recommendations system
==============================
Authors:
* Michal Matusiak
* Adam Szymanski

Movie recommendation problem
------------------------------
Let's create a system which recommends 5 movies for a given user
as well as informs about 5 NOT recommended movies.

While recommending the movie system bases on the input data in rating.json file
which contains users list along with movies they have watched & rating information
on the sale of 0 to 10.

System operates in the following steps:
1) Gets user name as an input.
2) Compares this user to all other users in the dataset (based on Pearson score)
    and sort them in descending order, so that we can get the most similar user to the "input user".
3) Get 5 recommended & 5 NOT recommended movies (excluding already watched moves by the "input user").
"""
import argparse
import json
import numpy as np

from compute_scores import euclidean_score


def build_arg_parser():
    parser = argparse.ArgumentParser(description='Find users who are similar to the input user')
    parser.add_argument('--user', dest='user', required=True,
                        help='Input user')
    return parser


# Get 5 recommended and not recommended movies
def get_recommended_movies(users, dataset, user):
    # Get user movie list
    user_movie_list = np.array(list(dataset[user].items()))

    recommended_movies = []
    not_recommended_movies = []
    # Get highest score user
    most_similar_user = users[0]

    # Get array of movies
    movie_list = np.array(list(dataset[most_similar_user[0]].items()))
    # Sort movies
    movie_list = movie_list[movie_list[:, 1].astype(int).argsort()]
    # Get lowest and highest score movies
    movie_highest_score = int(movie_list[-1][1])
    movie_lowest_score = int(movie_list[0][1])

    # Get exactly 5 elements of recommended movies
    counter = 0
    while len(recommended_movies) < 5:
        current_movie_list = movie_list[movie_list[:, 1] == str(movie_highest_score - counter)]
        if len(current_movie_list) > 0:
            recommended_movies.extend(current_movie_list.tolist())
        counter += 1
        # Check if user already watched the movie
        for i in recommended_movies:
            for j in user_movie_list:
                if str(i[0]) == str(j[0]):
                    recommended_movies.remove(i)
    recommended_movies = recommended_movies[:5]

    # Get exactly 5 elements of not recommended movies
    counter = 0
    while len(not_recommended_movies) < 5:
        current_movie_list = movie_list[movie_list[:, 1] == str(movie_lowest_score + counter)]
        if len(current_movie_list) > 0:
            not_recommended_movies.extend(current_movie_list.tolist())
        counter += 1
        # Check if user already watched the movie
        for i in not_recommended_movies:
            for j in user_movie_list:
                if i[0] == j[0]:
                    not_recommended_movies.remove(i)
    not_recommended_movies = not_recommended_movies[:5]

    return [recommended_movies, not_recommended_movies]


# Finds users in the dataset that are similar to the input user
def find_similar_users(dataset, user, num_users):
    if user not in dataset:
        raise TypeError('Cannot find ' + user + ' in the dataset')

    # Compute Pearson score between input user 
    # and all the users in the dataset
    scores = np.array([[x, euclidean_score(dataset, user,
                                           x)] for x in dataset if x != user])

    # Sort the scores in decreasing order
    scores_sorted = np.argsort(scores[:, 1])[::-1]

    # Extract the top 'num_users' scores
    top_users = scores_sorted[:num_users]

    return scores[top_users]


if __name__ == '__main__':
    args = build_arg_parser().parse_args()
    user = args.user

    ratings_file = 'rating.json'

    with open(ratings_file, 'r') as f:
        data = json.loads(f.read())

    similar_users = find_similar_users(data, user, len(data))

    movies = get_recommended_movies(similar_users, data, user)

    print("Recommended movies")
    print(movies[0])
    print('-' * 41)
    print("Not recommended movies")
    print(movies[1])
