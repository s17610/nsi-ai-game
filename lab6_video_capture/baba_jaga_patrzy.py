"""
=====================================================
Face & movement recognition system "Baba Jaga patrzy"
=====================================================
Authors:
* Michal Matusiak
* Adam Szymanski

To run program install:
- pip install numpy
- pip install opencv-python

-----------------------------------------------------

Let's create a system which:
a) recognizes a face & mark in real-time,
b) detects movement & mark it in different color (also in real-time).

System characteristics:
- Input: camera video capture,
- Face recognition based on HAAR filters,
- Green rectangle on not moving face,
- Red rectangle on moving face.

Source data for HAAR filters: https://github.com/opencv/opencv/tree/master/data/haarcascades
"""

import cv2
import numpy as np

# Load and check HAAR filter data for face recognition (frontal)
face_cascade = cv2.CascadeClassifier('haarcascades_data/haarcascade_frontalface_default.xml')

if face_cascade.empty():
    raise IOError('Unable to load the cascade classifier xml file')

# Video capture from camera0
cap = cv2.VideoCapture(0)

# Subtractor for foreground & backgroud comparison
fg_bg_subtractor = cv2.createBackgroundSubtractorMOG2(50, 25, False)

frameCount = 0

while True:
    _, frame = cap.read()
    frameCount += 1

    # Apply subtractor to frames from camera
    fg_mask = fg_bg_subtractor.apply(frame)

    # Count all the white (changing) pixels within the mask
    count = np.count_nonzero(fg_mask)
    print('Frame: %d, Pixel Count: %d' % (frameCount, count))

    # Convert colors to grayscale (due to face cascade usage)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    face_rects = face_cascade.detectMultiScale(gray, 1.3, 5)

    # Condition for how many pixels are considered as motion (changing pixels)
    if frameCount > 25 and count > 550:
        # Additional variables:
        text = 'Face movement detected'
        text_position = (15, 450)
        font_size = 1
        font_thickness = 2
        text_color = (0, 0, 255)
        cv2.putText(frame, text, text_position, cv2.FONT_HERSHEY_SIMPLEX,
                    font_size, text_color, font_thickness, cv2.LINE_AA)

        # Draw red rectangle on recognized moving face
        for (x, y, w, h) in face_rects:
            # Additional variables:
            move_shape_color = (0, 0, 255)
            move_shape_thickness = 2
            cv2.rectangle(frame, (x, y), (x + w, y + h), move_shape_color, move_shape_thickness)
    else:
        # Draw green rectangle on recognized not moving face
        for (x, y, w, h) in face_rects:
            # Additional variables:
            no_move_shape_color = (0, 255, 0)
            no_move_shape_thickness = 1
            cv2.rectangle(frame, (x, y), (x + w, y + h), no_move_shape_color, no_move_shape_thickness)

    # Display
    cv2.imshow('Baba Jaga patrzy', frame)

    # ESC to exit
    if cv2.waitKey(1) == 27:
        break

cap.release()
cv2.destroyAllWindows()
