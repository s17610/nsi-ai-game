"""
=====================================================
Mountain Car with Random Moves
=====================================================
Authors:
* Michal Matusiak
* Adam Szymanski

To run program install:
- pip install pyglet
- pip install gym[all]

Useful links: https://gym.openai.com/docs/
"""

import gym
env = gym.make('MountainCar-v0')
for i_episode in range(20):
    # range above defines number of games played (episodes)
    observation = env.reset()
    for t in range(100):
        env.render()
        print(observation)
        action = env.action_space.sample()
        observation, reward, done, info = env.step(action)
        if done:
            print("Episode finished after {} timesteps".format(t+1))
            # timestep = each action our agent takes
            # So there can be many timesteps in
            # Each timesteps returns:
            # a) observations (object) -> "state of environment" after timestep
            # b) reward (float) -> amount of reward received for previous action
            break
env.close()
