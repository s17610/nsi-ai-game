"""
==============================================
Diabetes recognition system for Pima Indians
==============================================
Authors:
* Michal Matusiak
* Adam Szymanski

To run program install:
- pip install numpy
- pip install scikit-learn
- pip install tensorflow
- pip install matplotlib
- pip install seaborn

Diabetes disease problem within Pima Indians
---------------------------------------------
With Pima Indians limitations to healthcare system, let's create a system which can help them
and predicts diabetes within 5 years.

System bases on data listed below.
Source of dataset: https://machinelearningmastery.com/standard-machine-learning-datasets/

System operates in the following steps:
1) Prepares input data, including:
- loading it from external file,
- separating it into 2 classes (class_0 -> healthy, class_1 -> sick),
2) Splits the input data into train & test datasets.
3) Creates keras model, including:
- training the model,
- testing the model.
4) Calculates accuracy of the model.
"""

import numpy as np
from sklearn.model_selection import train_test_split
import tensorflow as tf
from tensorflow import keras
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sn

fashion_mnist = tf.keras.datasets.fashion_mnist

# Load input data
input_file = 'data/pima_indians_diabetes.csv'
data = np.loadtxt(input_file, delimiter=',')
X, y = data[:, :-1], data[:, -1]

# Separate input data into two classes based on labels
class_0 = np.array(X[y == 0])
class_1 = np.array(X[y == 1])

# Split data into training and testing datasets
# 90% training and 10% test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=0)

# Example for 16 relu layers
number_of_layers = 16
model = keras.Sequential([
    keras.layers.Dense(number_of_layers, activation='relu'),
    keras.layers.Dense(10, activation='softmax')
])
model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
model.fit(X_train, y_train, epochs=10, verbose=0)

score, acc = model.evaluate(X_test, y_test, batch_size=len(y_train))
print('For', number_of_layers, 'layers')
print('Test score:', score)
print('Test accuracy:', acc)

# Example for 32 relu layers
number_of_layers = 32
model = keras.Sequential([
    keras.layers.Dense(number_of_layers, activation='relu'),
    keras.layers.Dense(10, activation='softmax')
])
model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
model.fit(X_train, y_train, epochs=10, verbose=0)

score, acc = model.evaluate(X_test, y_test, batch_size=len(y_train))
print('For', number_of_layers, 'layers')
print('Test score:', score)
print('Test accuracy:', acc)

# Get confusion matrix
predictions = model.predict(X_test)
y_classes = predictions.argmax(axis=-1)
matrix = confusion_matrix(y_test, y_classes)

print(y_classes)
print(y_test)

df_cm = pd.DataFrame(matrix,["True negative", "True positive"], ["negative", "positive"])
sn.set(font_scale=1.4) # for label size
sn.heatmap(df_cm, annot=True, annot_kws={"size": 16}) # font size
plt.show()
