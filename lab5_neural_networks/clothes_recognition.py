"""
==============================================
Clothes recognition
==============================================
Authors:
* Michal Matusiak
* Adam Szymanski

To run program install:
- pip install tensorflow
---------------------------------------------
Let's create a system which can help to recognize type of cloth in the image

System bases on data listed below.
Source of dataset: https://github.com/zalandoresearch/fashion-mnist
"""
import tensorflow as tf
from tensorflow import keras
fashion_mnist = tf.keras.datasets.fashion_mnist

(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()

model = keras.Sequential([
keras.layers.Flatten(input_shape=(28, 28)),
keras.layers.Dense(128, activation='relu'),
keras.layers.Dense(10, activation='softmax')
])
model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
model.fit(train_images, train_labels, epochs=10, verbose=0)
score, acc = model.evaluate(test_images, test_labels, batch_size=len(test_labels))
print('Test score:', score)
print('Test accuracy:', acc)
