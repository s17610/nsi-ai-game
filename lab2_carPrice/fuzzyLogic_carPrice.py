"""
==========================================
Fuzzy Control System: Car Price Estimator
==========================================
Authors:
* Michal Matusiak
* Adam Szymanski

To run program install
pip install scikit-fuzzy

Car Price Problem
-------------------
Let's create a fuzzy control system which models what should be price of a car.
While estimating the price we will consider 3 aspects:
- mileage (in thousands kilometers) on a scale of 0 to 200.
- age (in years) on a scale of 0 to 20.
- brand on a scale of 1 to 5 (1 is basic, eg. Fiat | 5 is premium, eg. Porsche)
Based on above we estimate the price (in thousands PLN) on a scale of 0 to 200.

* Antecednets (Inputs):
    - `mileage`
        * Universe (ie, crisp value range): What is mileage of the car (in thousands of kilometers),
        on a scale of 0 to 200?
        * Fuzzy set (ie, fuzzy value range): low, medium, high
    - `age`
        * Universe: How old is the car (in years), on a scale of 0 to 20?
        * Fuzzy set: young, average, old
    - `brand`
        * Universe: What is the car brand category, on a scale of 1 to 5 (1-basic, 5-premium)?
        * Fuzzy set: basic, standard, premium
* Consequents (Outputs):
    - `price`
        * Universe: What is estimated price (in thousands PLN), on a scale of 0 to 200?
        * Fuzzy set: low, medium, high
* Rules:
    - IF the *mileage* is medium *or* *age* is average *and* the *brand* is premium,
    THEN the price will be high.
    - IF the *mileage* is low *or* *age* is young *and* the *brand* is premium,
    THEN the price will be high.
    - IF the *mileage* is high *or* *age* is old *and* the *brand* is premium,
    THEN the price will be medium.
    - IF the *mileage* is low *and* *age* is young *and* the *brand* is standard,
    THEN the price will be high.
    - IF the *mileage* is low *and* *age* is young *and* the *brand* is basic,
    THEN the price will be medium.
    - IF the *mileage* is medium *and* *age* is average *and* the *brand* is standard *or* basic,
    THEN the price will be medium.
    - IF the *mileage* is high *and* *age* is old *and* the *brand* is standard,
    THEN the price will be medium.
    - IF the *mileage* is high *and* *age* is old *and* the *brand* is basic,
    THEN the price will be low.
"""
import matplotlib.pyplot as plt
import numpy as np
import skfuzzy as fuzz
from skfuzzy import control as ctrl

mileage = ctrl.Antecedent(np.arange(0, 201, 1), 'mileage')
age = ctrl.Antecedent(np.arange(0, 21, 1), 'age')
brand = ctrl.Antecedent(np.arange(1, 6, 1), 'brand')
price = ctrl.Consequent(np.arange(0, 201, 1), 'price')

mileage['low'] = fuzz.trimf(mileage.universe, [0, 0, 75])
mileage['medium'] = fuzz.trimf(mileage.universe, [0, 75, 150])
mileage['high'] = fuzz.trimf(mileage.universe, [75, 200, 200])

age['young'] = fuzz.trimf(age.universe, [0, 0, 5])
age['average'] = fuzz.trimf(age.universe, [0, 5, 10])
age['old'] = fuzz.trimf(age.universe, [5, 20, 20])

brand['basic'] = fuzz.trimf(brand.universe, [1, 1, 3])
brand['standard'] = fuzz.trimf(brand.universe, [1, 3, 5])
brand['premium'] = fuzz.trimf(brand.universe, [3, 5, 5])

price['low'] = fuzz.trimf(price.universe, [0, 0, 60])
price['medium'] = fuzz.trimf(price.universe, [0, 60, 120])
price['high'] = fuzz.trimf(price.universe, [60, 200, 200])

mileage['medium'].view()
age.view()
brand.view()
price.view()

rule1 = ctrl.Rule((mileage['medium'] | age['average']) & brand['premium'], price['high'])
rule2 = ctrl.Rule((mileage['low'] | age['young']) & brand['premium'], price['high'])
rule3 = ctrl.Rule((mileage['high'] | age['old']) & brand['premium'], price['medium'])
rule4 = ctrl.Rule(mileage['low'] & age['young'] & brand['standard'], price['high'])
rule5 = ctrl.Rule(mileage['low'] & age['young'] & brand['basic'], price['medium'])
rule6 = ctrl.Rule(mileage['medium'] & age['average'] & (brand['standard'] | brand['basic']), price['medium'])
rule7 = ctrl.Rule(mileage['high'] & age['old'] & brand['standard'], price['medium'])
rule8 = ctrl.Rule(mileage['high'] & age['old'] & brand['basic'], price['low'])

pricing_ctrl = ctrl.ControlSystem([rule1, rule2, rule3, rule4, rule5, rule6, rule7, rule8])
pricing = ctrl.ControlSystemSimulation(pricing_ctrl)

pricing.input['mileage'] = 129
pricing.input['age'] = 6
pricing.input['brand'] = 4

# Crunch the numbers
pricing.compute()

print(pricing.output['price'])
price.view(sim=pricing)

plt.show()
