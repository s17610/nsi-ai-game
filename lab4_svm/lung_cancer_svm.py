"""
===============================
Lung Cancer recognition system
===============================
Authors:
* Michal Matusiak
* Adam Szymanski

To run program install:
- pip install scikit-learn
- pip install numpy
- pip install matplotlib

Lung Cancer problem
------------------------------
Within COVID-19 pandemic & longer waiting times to get to the doctor with other diseases (which are
not COVID-19), let's create a system which can help to recognize Lung Cancer.

System bases on data listed below.
Source of dataset: https://www.kaggle.com/mysarahmadbhat/lung-cancer
1) Gender: 1(male), 0(female)
2) Age: Age of the patient
3) Smoking: YES=1, NO=0.
4) Yellow fingers: YES=1, NO=0.
5) Anxiety: YES=1, NO=0.
6) Peer_pressure: YES=1, NO=0.
7) Chronic Disease: YES=1, NO=0.
8) Fatigue: YES=1, NO=0.
9) Allergy: YES=1, NO=0.
10) Wheezing: YES=1, NO=0.
11) Alcohol: YES=1, NO=0.
12) Coughing: YES=1, NO=0.
13) Shortness of Breath: YES=1, NO=0.
14) Swallowing Difficulty: YES=1, NO=0.
15) Chest pain: YES=1, NO=0.
16) Lung Cancer: 1(YES), 0(NO).

System operates in the following steps:
1) Prepares input data, including:
- loading it from external file,
- separating it into 2 classes (class_0 -> healthy, class_1 -> sick),
- visualization,
2) Splits the input data into train & test datasets.
3) Creates SVM Classifier, including:
- training the model,
- testing the model.
4) Calculates accuracy of the model.
"""

import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn.model_selection import train_test_split
from sklearn import metrics

# Load input data
input_file = 'lung_cancer_data_modified.csv'
data = np.loadtxt(input_file, delimiter=',')
X, y = data[:, :-1], data[:, -1]

# Separate input data into two classes based on labels
class_0 = np.array(X[y == 0])
class_1 = np.array(X[y == 1])

# Visualize input data
plt.figure()
plt.scatter(class_0[:, 0], class_0[:, 1], s=75, facecolors='black', linewidth=1, marker='x')
plt.scatter(class_1[:, 0], class_1[:, 1], s=75, facecolors='white', edgecolors='black', linewidth=1, marker='o')
plt.title('Input data')
plt.show()

# Split data into training and testing datasets
# 75% training and 25% test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=5)

# Create a svm Classifier with linear kernel function
clf = svm.SVC(kernel='linear')

# Train the model using the training sets
clf.fit(X_train, y_train)

# Predict the response for test dataset
y_pred = clf.predict(X_test)

# Model Accuracy: how often is the classifier correct?
print('Accuracy:', metrics.accuracy_score(y_test, y_pred))
