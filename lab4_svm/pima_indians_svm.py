"""
==============================================
Diabetes recognition system for Pima Indians
==============================================
Authors:
* Michal Matusiak
* Adam Szymanski

To run program install:
- pip install scikit-learn
- pip install numpy
- pip install matplotlib

Diabetes disease problem within Pima Indians
---------------------------------------------
With Pima Indians limitations to healthcare system, let's create a system which can help them
and predicts diabetes within 5 years.

System bases on data listed below.
Source of dataset: https://machinelearningmastery.com/standard-machine-learning-datasets/
1) Number of times pregnant.
2) Plasma glucose concentration a 2 hours in an oral glucose tolerance test.
3) Diastolic blood pressure (mm Hg).
4) Triceps skinfold thickness (mm).
5) 2-Hour serum insulin (mu U/ml).
6) Body mass index (weight in kg/(height in m)^2).
7) Diabetes pedigree function.
8) Age (years).
9) Class variable (0 or 1).

System operates in the following steps:
1) Prepares input data, including:
- loading it from external file,
- separating it into 2 classes (class_0 -> healthy, class_1 -> sick),
- visualization,
2) Splits the input data into train & test datasets.
3) Creates SVM Classifier, including:
- training the model,
- testing the model.
4) Calculates accuracy of the model.
"""

import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn.model_selection import train_test_split
from sklearn import metrics

# Load input data
input_file = 'pima_indians_diabetes.csv'
data = np.loadtxt(input_file, delimiter=',')
X, y = data[:, :-1], data[:, -1]

# Separate input data into two classes based on labels
class_0 = np.array(X[y == 0])
class_1 = np.array(X[y == 1])

# Visualize input data
plt.figure()
plt.scatter(class_0[:, 0], class_0[:, 1], s=75, facecolors='black', linewidth=1, marker='x')
plt.scatter(class_1[:, 0], class_1[:, 1], s=75, facecolors='white', edgecolors='black', linewidth=1, marker='o')
plt.title('Input data')
plt.show()

# Split data into training and testing datasets
# 75% training and 25% test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=5)

# Create a svm Classifier with linear kernel function
clf = svm.SVC(kernel='linear')

# Train the model using the training sets
clf.fit(X_train, y_train)

# Predict the response for test dataset
y_pred = clf.predict(X_test)

# Model Accuracy: how often is the classifier correct?
print('Accuracy:', metrics.accuracy_score(y_test, y_pred))

